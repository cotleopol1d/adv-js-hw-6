const root = document.querySelector("#root");
const findBtn = document.querySelector(".find-button");
const urlIp = "https://api.ipify.org/?format=json";
const urlIpInfo = "http://ip-api.com/";

class General {
    constructor(urlIp, urlIpInfo) {
        this.urlIp = urlIp;
        this.urlIpInfo = urlIpInfo;
    }

    render(root) {
        const getInfo = async () => {
            try {
                const {ip} = await this.getIp();
                return await this.getInfo(ip);
            } catch (e) {
                throw e;
            }
        }
        getInfo().then(({continent, country, regionName, city, district}) => {
            root.innerHTML = "";
            const continentEl = document.createElement("p");
            const countryEl = document.createElement("p");
            const regionNameEl = document.createElement("p");
            const cityEl = document.createElement("p");
            const districtEl = document.createElement("p");
            continentEl.textContent = `Continent: "${continent}"`;
            countryEl.textContent = `Country: "${country}"`;
            regionNameEl.textContent = `Region: "${regionName}"`;
            cityEl.textContent = `City: "${city}"`;
            districtEl.textContent = `District: "${district}"`;
            root.append(continentEl);
            root.append(countryEl);
            root.append(regionNameEl);
            root.append(cityEl);
            root.append(districtEl);
        })
            .catch(e => {
                console.log(e.message);
            });
    }

    getIp() {
        const get = async () => {
            const response = await fetch(`${this.urlIp}`);
            if (!response.ok) {
                throw new Error(response.status);
            }
            return response.json();
        }
        return get();
    }

    getInfo(ip) {
        const get = async () => {
            const response = await fetch(`${this.urlIpInfo}json/${ip}?fields=status,message,continent,country,regionName,city,district`);
            if (!response.ok) {
                throw new Error(response.status);
            }
            return response.json();
        }
        return get();
    }

}

const general = new General(urlIp, urlIpInfo);

findBtn.addEventListener("click", () => {
    general.render(root);
})